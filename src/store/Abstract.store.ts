import MainStore, { IGetStore } from './MainStore'
import { observable, action } from 'mobx'

type Extend<T> = Omit<T, 'getStore'> & IGetStore

export class AbstractStore {
    @observable
    public mainStore!: Extend<MainStore>

    @action
    public setStore(store: MainStore) {
        this.mainStore = observable(store) as Extend<MainStore>
    }
}
