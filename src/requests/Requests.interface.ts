import { IMessage } from './../components/Mainchatpage/Dialog/store/Dialog.interface';


export interface IRegisterResponse {
    success: string
}
export interface ILoginResponse {
    success: string
    id: string
    token: string
}
export interface IAuthResponse {
    success: string
    isOnline: boolean
    id: string
    email: string
    name: string
    lastname: string
    avatarUrl: string
    contacts: []
    theme: string
}

export interface ISaveProfilePhotoResponse {
    avatarUrl: string
    _id: string
}

export interface IContactResponse {
    id: string
    name: string
    lastname: string
    email: string
    avatarUrl?: string
    hasNewMessage?: boolean
}

export interface ISetThemeReq {
    theme: string
    _id: string
}

export interface ISetThemeResponse {
    theme: string
}
//////////////////////////////////
/////    ADD CONTACTS
//////////////////////////////////
export interface IAddContactProps {
    clientId: string
    contactId: string
    //contactId: string
}

export interface IFindContactResponse {
    id: string
    avatarUrl: string
    name: string
    lastname: string
}

export interface IFindContactProps {
    email: string
}


export interface IDialogResponse {
    messages: IMessage[]
}

export interface IContact {
    contactId: string
    dialogId: string
}