import { IAddContactProps } from './../components/Mainchatpage/Contacts/AddContact'
import {
    IRegisterResponse,
    ILoginResponse,
    IAuthResponse,
    ISaveProfilePhotoResponse,
    IContactResponse,
    IDialogResponse,
    IFindContactResponse,
    IContact,
    ISetThemeReq,
    ISetThemeResponse,
} from './Requests.interface'
import { IUser } from './../components/Auth/store/Auth.interface'
import { IChangeSettingsProps } from './../components/Mainchatpage/SettingsBar/store/Settings.store'
import { getServerUrl } from '../env'

const baseUrl = getServerUrl()

export function registerClient(clientData: IUser) {
    return new Promise<IRegisterResponse>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/register`, {
            method: 'POST',
            body: JSON.stringify(clientData),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}

export function loginClientReq(clientData: IUser) {
    return new Promise<ILoginResponse>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/login`, {
            method: 'POST',
            body: JSON.stringify(clientData),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}

export function authClientReq(token: string) {
    return new Promise<IAuthResponse>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/auth`, {
            method: 'POST',
            body: JSON.stringify({ token }),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}

export function addProfilePhotoReq(data: FormData) {
    return new Promise<string>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/addAvatar`, {
            method: 'POST',
            body: data,
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}
export function saveProfilePhotoReq(data: ISaveProfilePhotoResponse) {
    return new Promise<string>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/saveAvatar`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}

export function updateClientSettings(data: IChangeSettingsProps) {
    return new Promise<IChangeSettingsProps>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/updateClient`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}

export function getContactById(contactId: string) {
    return new Promise<IContactResponse>((res, rej) => {
        return fetch(
            `${baseUrl}/api/users/getContactById?contactId=${contactId}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
            .then((res) => res.json())
            .then((parsedRes: IContactResponse) => {
                res(parsedRes)
            })
            .catch((err) => {
                rej(err)
            })
    })
}

export function getGalleryReq(dialogId: string) {
    return new Promise<string[]>((res, rej) => {
        return fetch(`${baseUrl}/api/users/getGallery?dialogId=${dialogId}`, {
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((parsedRes) => {
                res(parsedRes)
            })
            .catch((err) => {
                rej(err)
            })
    })
}

export function setThemeReq(data: ISetThemeReq) {
    return new Promise<ISetThemeResponse>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/setTheme`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}

//////////////////////////////////
/////    ADD CONTACTS
//////////////////////////////////

export function findContactReq(email: string) {
    return new Promise<IFindContactResponse>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/findContact`, {
            method: 'POST',
            body: JSON.stringify({ email }),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}

export function addContactReq(data: IAddContactProps) {
    return new Promise<IContactResponse>((resolve, reject) => {
        return fetch(`${baseUrl}/api/users/addContact`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            res.json().then((parsedRes) => {
                if (res.status === 200) {
                    resolve(parsedRes)
                } else {
                    reject(parsedRes)
                }
            })
        })
    })
}

export function findDialogById(dialogId: string) {
    return new Promise<IDialogResponse>((res, rej) => {
        return fetch(
            `${baseUrl}/api/users/getDialogById?dialogId=${dialogId}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
            .then((res) => res.json())
            .then((parsedRes: IDialogResponse) => {
                res(parsedRes)
            })
            .catch((err) => {
                rej(err)
            })
    })
}

export function deleteContactReq(data: IAddContactProps) {
    return new Promise<IContact[]>((resolve, rej) => {
        return fetch(`${baseUrl}/api/users/deleteContact`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((parsedRes) => {
                resolve(parsedRes)
            })
            .catch((err) => {
                rej(err)
            })
    })
}
