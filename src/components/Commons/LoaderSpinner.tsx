import * as React from 'react'
import Loader from 'react-loader-spinner'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'

export interface ILoaderSpinnerProps {
    size?: number
    color?: string
}

export class LoaderSpinner extends React.Component<ILoaderSpinnerProps> {
    render() {
        const { size, color = '#4d7d96' } = this.props
        return (
            <div className="spinner-wrapper">
                <Loader
                    type={'ThreeDots'}
                    color={color}
                    width={size}
                    height={size}
                />
            </div>
        )
    }
}
