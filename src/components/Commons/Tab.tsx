import * as React from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
export interface ITabComponents {
    title: string
    name: string
    component: JSX.Element | JSX.Element[]
    className: string
}

export interface ITabProps {
    tabs: ITabComponents[]
}

@observer
export default class Tab extends React.Component<ITabProps> {
    @observable
    private active: string = ''

    constructor(props: ITabProps) {
        super(props)
        this.active = this.props.tabs[0].name
    }

    @action.bound
    private onClickChangeTab(e: React.SyntheticEvent<HTMLDivElement>) {
        if(e.currentTarget.dataset.id) {
            this.active = e.currentTarget.dataset.id
        }
    }

    public render() {
        return this.props.tabs.map((tab) => {
            const { title, component, name, className } = tab
            return (
                <div key={title} >
                    <div data-id={name}  onClick={this.onClickChangeTab}>{title}</div>
                    {this.active === name && <div className={className}>{component}</div>}
                </div>
            )
        })
    }
}
