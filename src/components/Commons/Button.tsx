import * as React from 'react'
import { LoaderSpinner } from './LoaderSpinner'
import { observer } from 'mobx-react'
import { observable } from 'mobx'

export interface IButtonProps {
    text: string
    isPrimary?: boolean
    isLoading?: boolean
}

@observer
export class Button extends React.Component<IButtonProps> {
    @observable
    private buttonElement?: React.RefObject<
        HTMLLabelElement
    > = React.createRef()

    @observable
    private height?: number

    @observable
    private width?: number

    componentDidMount() {
        if (this.buttonElement) {
            const elem = this.buttonElement.current

            this.width = elem?.offsetWidth
            this.height = elem?.offsetHeight
        }
    }

    componentDidUpdate() {
        const elem = document.getElementById('submit-label')

        if (this.props.isLoading) {
            if (elem && this.width && this.height) {
                elem.style.width = `${this.width}px`
                elem.style.height = `${this.height}px`
            }
        } else {
            if (elem) {
                elem.style.width = ''
                elem.style.height = ''
            }
        }
    }

    render() {
        const { text, isPrimary, isLoading } = this.props
        return (
            <>
                <input id="submit-button" type="submit" />
                <label
                    id="submit-label"
                    ref={this.buttonElement}
                    htmlFor="submit-button"
                    className={`button ${isPrimary ? 'button--primary' : ''} ${
                        isLoading ? 'button--loading' : ''
                    }`}
                >
                    {isLoading ? (
                        <LoaderSpinner size={25} color={'#fff'} />
                    ) : (
                        text
                    )}
                </label>
            </>
        )
    }
}
