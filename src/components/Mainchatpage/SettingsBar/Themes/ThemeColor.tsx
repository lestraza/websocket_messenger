import * as React from 'react'
import { observable, action } from 'mobx'
import { IGetStore } from '../../../../store/MainStore'
import { LoaderSpinner } from '../../../Commons/LoaderSpinner'
import { inject, observer } from 'mobx-react'

export interface IThemeColorProps {
    color: string
    key: string
}

@inject('getStore')
@observer
export default class ThemeColor extends React.Component<IThemeColorProps> {
    authStore = this.injected.getStore('authStore')

    private get injected() {
        return this.props as IThemeColorProps & IGetStore
    }
    @observable
    private isLoadingTheme: boolean = false

    @action.bound
    private onClickSetTheme(e: React.SyntheticEvent<HTMLDivElement>) {
        if (e.currentTarget.dataset.id) {    
            const theme = e.currentTarget.dataset.id
            const { setTheme } = this.authStore
            this.isLoadingTheme = true
            setTheme(theme).then(() => {
                this.isLoadingTheme = false
            })
        }
    }

    public render() {
        const { color, key } = this.props
        return (
            <div
                style={{ backgroundColor: color }}
                className="theme__item"
                key={key}
                data-id={color}
                onClick={this.onClickSetTheme}
            >
                {this.isLoadingTheme && (
                    <LoaderSpinner size={30} color="#4d7d96" />
                )}
            </div>
        )
    }
}
