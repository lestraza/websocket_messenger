import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { IGetStore } from '../../../../store/MainStore'
import ThemeColor from './ThemeColor'

const colors: string[] = [
    '#cddc393b',
    '#ffeb3b2b',
    '#9e576f38',
    '#71a2bb63',
    '#9e9e9e47',
    '#ff98002e',
]

export interface IThemeSettingsProps {}

@inject('getStore')
@observer
export default class ThemeSettings extends React.Component<
    IThemeSettingsProps
> {
    authStore = this.injected.getStore('authStore')

    private get injected() {
        return this.props as IThemeSettingsProps & IGetStore
    }

    public render() {
        return colors.map((color) => {
            return <ThemeColor key={color} color={color} />
        })
    }
}
