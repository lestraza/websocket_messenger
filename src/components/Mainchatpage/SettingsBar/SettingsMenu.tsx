import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { IGetStore } from '../../../store/MainStore'
import Tab from '../../Commons/Tab'
import SettingsClientData from './SettingsClientData'
import ThemeSettings from './Themes/ThemeSettings'

export interface ISettingsMenuProps {}

@inject('getStore')
@observer
export default class SettingsMenu extends React.Component<ISettingsMenuProps> {
    authStore = this.injected.getStore('authStore')

    private get injected() {
        return this.props as ISettingsMenuProps & IGetStore
    }

    public render() {
        return (
            <div>
                <Tab
                    tabs={[
                        {
                            title: 'Profile',
                            name: 'profile',
                            component: <SettingsClientData />,
                            className: ''
                        },
                        {
                            title: 'Theme',
                            name: 'theme',
                            component: <ThemeSettings />,
                            className: 'theme'
                        },
                    ]}
                />
            </div>
        )
    }
}
