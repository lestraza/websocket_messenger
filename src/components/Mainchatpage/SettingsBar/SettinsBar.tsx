import * as React from 'react'
import SettingsBarHeader from './SettingsBarHeader'
import { inject, observer } from 'mobx-react'
import { IGetStore } from '../../../store/MainStore'
import SettingsMenu from './SettingsMenu'

export interface ISettingsBarProps {}

@inject('getStore')
@observer
export default class SettingsBar extends React.Component<ISettingsBarProps> {
    authStore = this.injected.getStore('authStore')

    private get injected() {
        return this.props as ISettingsBarProps & IGetStore
    }

    public render() {
        return (
            <div className="client-settings">
                <SettingsBarHeader />
                <SettingsMenu />
            </div>
        )
    }
}
