import React, { Component } from 'react'
import { action, computed } from 'mobx'
import { inject, observer } from 'mobx-react'
import { IGetStore } from '../../../store/MainStore'
import MessageScreenContainer from './MessageScreenContainer'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPaperPlane, faPaperclip } from '@fortawesome/free-solid-svg-icons'
import { faSmile } from '@fortawesome/free-regular-svg-icons'
import AddContact from '../Contacts/AddContact'
import DialogHeader from './DialogHeader'
import PreviewModal from './PreviewModal'
import ImagesGallery from './ImagesGallery/ImagesGallery'

export interface IDialogProps {}
@inject('getStore')
@observer
export default class Dialog extends Component<IDialogProps> {
    mainStore = this.injected.getStore('mainStore')
    dialogStore = this.injected.getStore('dialogStore')
    authStore = this.injected.getStore('authStore')

    private get injected() {
        return this.props as IDialogProps & IGetStore
    }

    @computed
    private get style() {
        return { backgroundColor: this.authStore.client.theme }
    }

    @action.bound
    formSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        this.dialogStore.sendMessage()
    }

    @action.bound
    onChangeSaveValue(e: React.FormEvent<HTMLInputElement>) {
        const { saveNewMessage } = this.dialogStore
        const value = e.currentTarget.value
        saveNewMessage(value)
    }

    @action.bound
    private onChangePreviewFile(event: React.SyntheticEvent<HTMLInputElement>) {
        const { getBase64 } = this.dialogStore
        const target = event?.currentTarget
        const file = target.files ? target.files[0] : undefined

        if (file) {
            this.dialogStore.currentFile.file = file.name
            getBase64(file)
                .then((encodedFile) => {
                    this.dialogStore.currentFile.file = encodedFile
                    this.dialogStore.isShowFilePreview = true
                })
                .catch((err) => {
                    this.mainStore.error = err
                })
        }
    }

    public render() {
        const {
            newMessage,
            isShowFilePreview,
            isShowAddContactModal,
            isShowGallery,
            currentContact,
        } = this.dialogStore
        return (
            <div className="dialog">
                <DialogHeader />
                <div className="dialog__screen" style={this.style}>
                    <MessageScreenContainer />
                    {isShowFilePreview && (
                        <PreviewModal
                            file={this.dialogStore.currentFile.file}
                        />
                    )}
                </div>
                <form
                    className="dialog__type-message"
                    onSubmit={this.formSubmit}
                >
                    <input
                        type="text"
                        placeholder="Enter your message"
                        onChange={this.onChangeSaveValue}
                        className="dialog__input"
                        value={newMessage}
                    />
                    <input id={'submit'} type="submit"></input>
                    <label
                        htmlFor="submit"
                        className="dialog__send-message-button"
                        title="Send"
                    >
                        <FontAwesomeIcon icon={faPaperPlane} color={'#FFF'} />
                    </label>
                    <div className="dialog__emodji">
                        <FontAwesomeIcon
                            icon={faSmile}
                            color={'#bababa'}
                            size={'lg'}
                        />
                    </div>
                    {currentContact.id && (
                        <div className="dialog__clip">
                            <label htmlFor="">
                                <FontAwesomeIcon
                                    icon={faPaperclip}
                                    color={'#bababa'}
                                    size={'lg'}
                                />
                            </label>
                            <input
                                type="file"
                                id="load-file"
                                onChange={this.onChangePreviewFile}
                            />
                        </div>
                    )}
                </form>
                {isShowGallery && <ImagesGallery />}
                {isShowAddContactModal && <AddContact />}
            </div>
        )
    }
}
