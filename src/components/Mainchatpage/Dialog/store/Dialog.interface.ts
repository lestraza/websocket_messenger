export interface IUserContacts {
    contactId: string
    dialogId: string
}

export interface IMessage {
    dialogId?: string
    contactId: string
    id: string
    name: string
    lastname: string
    fileName?: string
    text: string
    timeStamp: string
    imageUrl?: string
}

export interface IFileToSend {
    file: string
    fileName: string
}
