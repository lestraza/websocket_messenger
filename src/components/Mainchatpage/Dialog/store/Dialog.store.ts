import {
    findDialogById,
    deleteContactReq,
    getGalleryReq,
} from './../../../../requests/index'
import { IContactResponse } from './../../../../requests/Requests.interface'

import { IMessage, IFileToSend } from './Dialog.interface'
import { IUser } from './../../../Auth/store/Auth.interface'
import { AbstractStore } from './../../../../store/Abstract.store'
import { action, observable, runInAction, computed } from 'mobx'
import {
    addContactReq,
    findContactReq,
    getContactById,
} from '../../../../requests'

const initContact = {
    email: '',
    id: '',
    name: '',
    lastname: '',
    avatarUrl: '',
}

export class DialogStore extends AbstractStore {
    @observable
    public newContact: IUser = { ...initContact }

    @observable
    public isChatListLoading: boolean = false

    @observable
    public isHistoryLoading: boolean = false

    @observable
    public contacts: IContactResponse[] = []

    @observable
    isShowAddContactModal: boolean = false

    @observable
    dialogStoreServerError: string = ''

    @observable
    public currentDialog: IMessage[] = []

    @observable
    public currentDialogId: string = ''

    @observable
    public currentContact: IUser = {}

    @observable
    public newMessage: string = ''

    @observable
    public isAlreadyInContacts: boolean = false

    @observable
    public isShowDialogMenu: boolean = false

    @observable
    public currentFile: IFileToSend = {
        file: '',
        fileName: '',
    }
    @observable
    public isShowFilePreview: boolean = false

    @observable
    public imagesFromDialog: string[] = []

    @observable
    public isShowGallery: boolean = false

    @observable
    public isActiveImage: string = ''

    @observable
    public indent: string = '-1000px'

    @observable
    public isShowRightGalleryHandler: boolean = false

    @observable
    public isShowLeftGalleryHandler: boolean = true

    @computed
    private get authStore() {
        return this.mainStore.getStore('authStore')
    }

    @action.bound
    scrollToItem(item: HTMLElement) {
        const lastMessage = this.currentDialog[this.currentDialog.length - 1]
        if (lastMessage.text === item.innerText) {
            item.scrollIntoView()
        }
    }

    @action.bound
    public showOrCloseAddContactModal() {
        if (this.isShowAddContactModal) {
            this.isShowAddContactModal = false
            this.newContact = { ...initContact }
            this.mainStore.error = ''
        } else {
            this.isShowAddContactModal = true
        }
    }

    @action.bound
    public searchNewContactEmail(email: string) {
        this.isAlreadyInContacts = this.contacts.some(
            (contact) => contact.email === email.trim()
        )
        this.newContact.email = email.trim()
        if (this.isAlreadyInContacts) {
            this.mainStore.error = 'Contact already added'
        } else if (email === this.authStore.client.email) {
            this.mainStore.error = 'Try other email!'
        } else {
            this.mainStore.error = ''
        }
    }

    @action.bound
    public findContactByEmail(): Promise<void> {
        if (this.isAlreadyInContacts) {
            return Promise.reject()
        } else {
            return findContactReq(this.newContact.email || '').then((res) => {
                runInAction(() => {
                    this.newContact.id = res.id
                    this.newContact.name = res.name
                    this.newContact.lastname = res.lastname
                    this.isShowAddContactModal = true
                    this.mainStore.error = ''
                })
            })
        }
    }

    @action.bound
    public getContactsById() {
        const { contacts } = this.authStore.client
        this.isChatListLoading = true

        if (contacts) {
            return Promise.all(
                contacts.map((contact) => {
                    return getContactById(contact.contactId)
                })
            )
                .then((contacts) => {
                    runInAction(() => {
                        this.contacts = contacts
                        this.newContact = { ...initContact }
                        this.mainStore.error = ''
                    })
                })
                .catch((err) => {
                    this.mainStore.error = err.error
                })
                .finally(() => {
                    runInAction(() => {
                        this.isChatListLoading = false
                    })
                })
        }
    }

    @action.bound
    public addContact() {
        const data = {
            clientId: this.authStore.client.id || '',
            contactId: this.newContact.id || '',
        }
        addContactReq(data)
            .then((contact) => {
                runInAction(() => {
                    this.currentContact = { ...contact }
                    this.isShowAddContactModal = false
                    this.newContact = { ...initContact }
                    this.contacts = [...this.contacts, contact]
                    this.mainStore.error = ''
                })
            })
            .catch((err) => {
                this.mainStore.error = err.error
            })
    }

    @action.bound
    public saveNewMessage(message: string) {
        this.newMessage = message
    }

    @action.bound
    public getContactHistory(contactId: string) {
        const { socket } = this.authStore
        this.isHistoryLoading = true
        const selectedContact = this.authStore.client.contacts?.find(
            (contact) => contact.contactId === contactId
        )
        if (this.currentDialogId) {
            socket?.emit('leaveChat', this.currentDialogId)
            this.currentDialog = []
        }
        if (selectedContact) {
            const { contactId, dialogId } = selectedContact
            this.currentContact.id = contactId
            this.currentDialogId = dialogId
            findDialogById(dialogId)
                .then((dialog) => {
                    runInAction(() => {
                        socket?.emit('joinChat', dialogId)
                        this.currentDialog = [...(dialog.messages || [])]
                        this.contacts.forEach((contact) => {
                            if (contact.id === contactId) {
                                contact.hasNewMessage = false
                                this.mainStore.error = ''
                            }
                        })
                    })
                })
                .catch((err) => {
                    this.mainStore.error = err.error
                })
                .finally(() => {
                    runInAction(() => {
                        this.isHistoryLoading = false
                    })
                })
        }
    }

    @action.bound
    public sendMessage() {
        const { id, name, lastname } = this.authStore.client
        const { socket } = this.authStore

        if (id && name && lastname && this.currentContact.id) {
            const dialogMessage: IMessage = {
                contactId: this.currentContact.id,
                id,
                name,
                lastname,
                text: this.newMessage,
                timeStamp: new Date().toISOString(),
                dialogId: this.currentDialogId,
            }
            if (dialogMessage && socket) {
                this.currentDialog = [...this.currentDialog, dialogMessage]
                socket.emit('sendMessage', dialogMessage)
            }
            this.newMessage = ''
        }
    }

    @action.bound
    sendNotification(id: string) {
        this.contacts.forEach((contact) => {
            if (contact.id === id) {
                contact.hasNewMessage = true
            }
        })
    }

    public getBase64(file: File) {
        return new Promise<string>((resolve, reject) => {
            const reader = new FileReader()
            reader.readAsDataURL(file)
            reader.onload = function () {
                resolve(reader.result as string)
            }
            reader.onerror = function (error) {
                reject(error)
            }
        })
    }

    @action.bound
    public sendFile() {
        const { socket } = this.authStore
        const { id, name, lastname } = this.authStore.client
        if (id && name && lastname && this.currentContact.id) {
            const dialogMessage: IMessage = {
                contactId: this.currentContact.id,
                id,
                name,
                lastname,
                fileName: this.currentFile.fileName,
                text: '',
                imageUrl: this.currentFile.file,
                timeStamp: new Date().toISOString(),
                dialogId: this.currentDialogId,
            }
            if (socket) {
                this.currentDialog = [...this.currentDialog, dialogMessage]
                socket.emit('sendFile', dialogMessage)
                this.currentFile = {
                    file: '',
                    fileName: '',
                }
            }
        }
    }

    @action.bound
    public showGalleryImages() {
        getGalleryReq(this.currentDialogId)
            .then((res) => {
                this.imagesFromDialog = res
                this.isShowGallery = true
            })
            .catch((err) => {
                this.mainStore.error = err.error
            })
    }

    @action.bound
    private galleryHandler() {
        const index = this.imagesFromDialog.indexOf(this.isActiveImage)
        if (index <= 0) {
            this.isShowLeftGalleryHandler = false
        } else {
            this.isShowLeftGalleryHandler = true
        }
        if (index < this.imagesFromDialog.length - 1) {
            this.isShowRightGalleryHandler = true
        } else {
            this.isShowRightGalleryHandler = false
        }
    }

    @action.bound
    public showPreviousImage() {
        const currentImgIndex = this.imagesFromDialog.indexOf(
            this.isActiveImage
        )
        if (currentImgIndex > 0) {
            const nextImgIndex = currentImgIndex - 1
            const spaceBetweenImages =
                parseInt(this.indent.split('px')[0]) + 182
            this.indent = spaceBetweenImages + 'px'
            this.isActiveImage = this.imagesFromDialog[nextImgIndex]
            this.galleryHandler()
        }
    }

    @action.bound
    public showNextImage() {
        const currentImgIndex = this.imagesFromDialog.indexOf(
            this.isActiveImage
        )
        if (currentImgIndex < this.imagesFromDialog.length - 1) {
            const nextImgIndex = currentImgIndex + 1
            const spaceBetweenImages =
                parseInt(this.indent.split('px')[0]) - 182
            this.indent = spaceBetweenImages + 'px'
            this.isActiveImage = this.imagesFromDialog[nextImgIndex]
            this.galleryHandler()
        }
    }

    @action.bound
    public selectImage(image: string) {
        const currentImgIndex = this.imagesFromDialog.indexOf(
            this.isActiveImage
        )
        const selectImgIndex = this.imagesFromDialog.indexOf(image)
        const difference = currentImgIndex - selectImgIndex
        const spaceBetweenImages =
            parseInt(this.indent.split('px')[0]) + difference * 182
        this.indent = spaceBetweenImages + 'px'
        this.isActiveImage = image
        this.galleryHandler()
    }

    @action.bound
    public deleteContact() {
        const data = {
            clientId: this.authStore.client.id || '',
            contactId: this.currentContact.id || '',
        }
        deleteContactReq(data)
            .then((res) => {
                runInAction(() => {
                    this.contacts = this.contacts.filter((contact) => {
                        return contact.id !== data.contactId
                    })
                    this.currentDialog = []
                    this.isShowDialogMenu = false
                    this.authStore.client.contacts = [...res]
                    this.currentContact = { ...initContact }
                    this.mainStore.error = ''
                })
            })
            .catch((err) => {
                this.mainStore.error = err.error
            })
    }
}
