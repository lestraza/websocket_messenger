import * as React from 'react'
import { IGetStore } from '../../../store/MainStore'
import { action } from 'mobx'
import { inject, observer } from 'mobx-react'
import Message from './Message'
import { LoaderSpinner } from '../../Commons/LoaderSpinner'

export interface IMessageScreenContainerProps {}

@inject('getStore')
@observer
export default class MessageScreenContainer extends React.Component<
    IMessageScreenContainerProps
> {
    dialogStore = this.injected.getStore('dialogStore')

    private get injected() {
        return this.props as IMessageScreenContainerProps & IGetStore
    }

    componentWillUnmount() {
        const { isShowFilePreview } = this.dialogStore
        if (!isShowFilePreview) {
            this.dialogStore.currentDialog = []
        }
    }

    @action.bound
    renderScreenContainers() {
        return this.dialogStore.currentDialog.map((message) => {
            return <Message message={message} key={message.timeStamp} />
        })
    }
    public render() {
        const { currentDialog, isHistoryLoading } = this.dialogStore
        return (
            <>
                {!isHistoryLoading ? (
                    currentDialog.length > 0 && this.renderScreenContainers()
                ) : (
                    <LoaderSpinner size={60} />
                )}
            </>
        )
    }
}
