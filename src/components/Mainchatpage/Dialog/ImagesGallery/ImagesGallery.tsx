import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { IGetStore } from '../../../../store/MainStore'
import { action, computed, observable } from 'mobx'
import DemoImage from './DemoImage'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faTimes,
    faChevronRight,
    faChevronLeft,
} from '@fortawesome/free-solid-svg-icons'

export interface IImagesGalleryProps {}

@inject('getStore')
@observer
export default class ImagesGallery extends React.Component<
    IImagesGalleryProps
> {
    dialogStore = this.injected.getStore('dialogStore')

    private get injected() {
        return this.props as IImagesGalleryProps & IGetStore
    }

    constructor(props: IImagesGalleryProps) {
        super(props)
        const { imagesFromDialog } = this.dialogStore
        this.dialogStore.isActiveImage =
            imagesFromDialog[imagesFromDialog.length - 1]
    }

    @observable
    private right: string = '0px'

    @observable
    private images: number = 0

    @computed
    private get backgroundMainImage() {
        return {
            backgroundImage: `url(${this.dialogStore.isActiveImage})`,
        }
    }

    @computed
    private get indent() {
        return {
            left: this.dialogStore.indent,
        }
    }

    @action.bound
    public onClickSelectImage(image: string) {
        this.dialogStore.isActiveImage = image
    }

    @action.bound
    private onClickCloseGallery() {
        this.dialogStore.isShowGallery = false
    }

    @action.bound
    private renderImageGallery() {
        const { imagesFromDialog } = this.dialogStore
        this.images = imagesFromDialog.length + 1
        return imagesFromDialog.map((image) => {
            return <DemoImage image={image} key={image} />
        })
    }

    @action.bound
    private onClickPreviousImage() {
        this.dialogStore.showPreviousImage()
    }

    @action.bound
    private onClickNextImage() {
        this.dialogStore.showNextImage()
    }

    public render() {
        const {
            isShowLeftGalleryHandler,
            isShowRightGalleryHandler,
        } = this.dialogStore
        return (
            <div className="image-gallery">
                <div
                    className="image-gallery__close"
                    onClick={this.onClickCloseGallery}
                >
                    <FontAwesomeIcon
                        icon={faTimes}
                        size={'2x'}
                        color={'#8a9aa2'}
                    />
                </div>
                {isShowRightGalleryHandler && (
                    <div
                        className="image-gallery__chevron-right"
                        onClick={this.onClickNextImage}
                    >
                        <FontAwesomeIcon
                            icon={faChevronRight}
                            size={'2x'}
                            color={'#8a9aa2'}
                        />
                    </div>
                )}

                {isShowLeftGalleryHandler && (
                    <div
                        className="image-gallery__chevron-left"
                        onClick={this.onClickPreviousImage}
                    >
                        <FontAwesomeIcon
                            icon={faChevronLeft}
                            size={'2x'}
                            color={'#8a9aa2'}
                        />
                    </div>
                )}

                <div
                    className="image-gallery__image"
                    style={this.backgroundMainImage}
                ></div>
                <div className="image-gallery__wrapper">
                    <div
                        className="image-gallery__demo-container"
                        style={this.indent}
                    >
                        {this.renderImageGallery()}
                    </div>
                </div>
            </div>
        )
    }
}
