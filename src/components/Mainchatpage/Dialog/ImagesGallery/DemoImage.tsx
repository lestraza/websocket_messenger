import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { IGetStore } from '../../../../store/MainStore'
import { computed, action } from 'mobx'

export interface IDemoImageProps {
    image: string
}

@inject('getStore')
@observer
export default class DemoImage extends React.Component<IDemoImageProps> {
    dialogStore = this.injected.getStore('dialogStore')

    private get injected() {
        return this.props as IDemoImageProps & IGetStore
    }

    @computed
    private get backgroundImage() {
        return {
            backgroundImage: `url(${this.props.image})`,
        }
    }

    @action.bound
    private onClickImage() {
        this.dialogStore.selectImage(this.props.image)
    }

    public render() {
        const { image } = this.props
        return (
            <div
                className={`image-gallery__demo-image-container ${
                    this.dialogStore.isActiveImage === image && 'active'
                }`}
                onClick={this.onClickImage}
                style={this.backgroundImage}
            ></div>
        )
    }
}
