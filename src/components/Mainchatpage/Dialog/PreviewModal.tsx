import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons'
import { action } from 'mobx'
import { inject, observer } from 'mobx-react'
import { IGetStore } from '../../../store/MainStore'

export interface IPreviewModalProps {
    file: string
}

@inject('getStore')
@observer
export default class PreviewModal extends React.Component<IPreviewModalProps> {
    dialogStore = this.injected.getStore('dialogStore')
    authStore = this.injected.getStore('authStore')

    private get injected() {
        return this.props as IPreviewModalProps & IGetStore
    }

    @action.bound
    onChangeSendFile() {
        this.dialogStore.isShowFilePreview = false
        this.dialogStore.sendFile()
    }

    public render() {
        const { file } = this.props
        return (
            <div className="preview-modal__container">
                <div className="preview-modal__header">
                    <img
                        src={file}
                        alt="avatar_image"
                        className="preview-modal__image"
                    />
                </div>
                <input type="text" className="preview-modal__caption" />
                <input
                    id={'submit'}
                    type="submit"
                    className="preview-modal__input-submit"
                />
                <label onClick={this.onChangeSendFile} htmlFor="submit">
                    <FontAwesomeIcon
                        icon={faPaperPlane}
                        color={'#FFF'}
                        className="preview-modal__button"
                    />
                </label>
            </div>
        )
    }
}
