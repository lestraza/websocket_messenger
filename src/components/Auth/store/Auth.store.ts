import { ISetThemeReq } from './../../../requests/Requests.interface'
import { IMessage } from './../../Mainchatpage/Dialog/store/Dialog.interface'
import { IUser } from './Auth.interface'
import { AbstractStore } from './../../../store/Abstract.store'
import {
    saveProfilePhotoReq,
    addProfilePhotoReq,
    setThemeReq,
} from './../../../requests/index'
import { ISaveProfilePhotoResponse } from '../../../requests/Requests.interface'
import { action, observable, runInAction, reaction } from 'mobx'
import {
    registerClient,
    loginClientReq,
    authClientReq,
    updateClientSettings,
} from '../../../requests'
import io from 'socket.io-client'

export class AuthStore extends AbstractStore {
    @observable
    public isAuthorizing: boolean = false

    @observable
    public isSignLoading: boolean = false

    @observable
    public isRegisterLoading: boolean = false

    @observable
    public socket?: SocketIOClient.Socket

    @observable
    public clientRegisterProps: IUser = {
        name: '',
        lastname: '',
        email: '',
        password: '',
        avatarUrl: '',
    }

    @observable
    public client: IUser = {
        id: '',
        name: '',
        lastname: '',
        email: '',
        avatarUrl: '',
        contacts: [],
        isOnline: false,
        theme: '',
    }

    @observable
    public isAuthenticated: boolean = false

    @observable
    public clientId: string = ''

    @observable
    public isAuthenticatedByToken: boolean = false

    @observable
    newSettings: IUser = {}

    @observable
    public isShowSettingsBar: boolean = false

    private get dialogStore() {
        return this.mainStore.getStore('dialogStore')
    }

    constructor() {
        super()

        this.isAuthorizing = true
        this.authClient().finally(() => {
            runInAction(() => {
                this.isAuthorizing = false
            })
        })

        reaction(
            () => this.client,
            () => {
                this.newSettings = { ...this.client }
            }
        )
    }

    @action.bound
    public saveInputValueRegisterForm(prop: keyof IUser, value: string) {
        if (prop) {
            ;(this.clientRegisterProps[prop] as string) = value
        }
    }

    @action.bound
    public registerNewClient(): Promise<void> {
        this.isRegisterLoading = true

        return registerClient(this.clientRegisterProps)
            .then(() => {
                runInAction(() => {
                    this.clientRegisterProps = {
                        name: '',
                        lastname: '',
                        email: '',
                        password: '',
                        avatarUrl: '',
                    }
                    this.mainStore.error = ''
                })
            })
            .finally(() => {
                runInAction(() => {
                    this.isRegisterLoading = false
                })
            })
    }

    @action.bound
    public clientLogin() {
        return new Promise((resolve, reject) => {
            loginClientReq(this.clientRegisterProps)
                .then((res) => {
                    runInAction(() => {
                        this.clientRegisterProps = {
                            email: '',
                            password: '',
                        }
                        localStorage.setItem('user_token', res.token)
                        this.clientId = res.id
                        this.mainStore.error = ''
                        this.authClient().then(() => {
                            resolve()
                        })
                    })
                })
                .catch((err) => {
                    runInAction(() => {
                        this.mainStore.error = err.error
                    })
                    reject()
                })
        })
    }

    @action.bound
    public authClient() {
        const token = localStorage.getItem('user_token')
        if (token) {
            return authClientReq(token)
                .then((res) => {
                    runInAction(() => {
                        this.client = {
                            id: res.id,
                            name: res.name,
                            lastname: res.lastname,
                            email: res.email,
                            avatarUrl: res.avatarUrl,
                            contacts: res.contacts,
                            isOnline: res.isOnline,
                            theme: res.theme,
                        }
                        this.isAuthenticated = true
                        this.dialogStore.getContactsById()
                        this.connectSocket()
                    })
                })
                .catch(() => {
                    runInAction(() => {
                        this.isAuthenticated = false
                        this.socket?.emit('disconnect')
                    })
                })
        } else {
            this.isAuthenticated = false
            this.socket?.emit('disconnect')
            return Promise.resolve()
        }
    }

    @action.bound
    private connectSocket() {
        this.socket = io(this.mainStore.serverUrl, {
            query: {
                id: this.client.id,
            },
        })

        this.socket.on('connect', () => {
            console.log('connected')
        })
        this.socket.on('receiveMessage', (dialogMessage: IMessage) => {
            const { currentDialog } = this.dialogStore
            currentDialog.push(dialogMessage)
        })
        this.socket.on('inviteToChat', (id: string) => {
            const { sendNotification } = this.dialogStore
            sendNotification(id)
        })
        this.socket.on('receiveFile', (dialogMessage: IMessage) => {
            this.dialogStore.currentDialog = [
                ...this.dialogStore.currentDialog,
                dialogMessage,
            ]
        })
        this.socket.on('error', (error: string) => {
            this.mainStore.error = error
        })
    }

    @action.bound
    public logout() {
        localStorage.setItem('user_token', '')
        this.authClient()
    }

    @action.bound
    public showOrCloseSettingsBar() {
        if (this.isShowSettingsBar) {
            this.isShowSettingsBar = false
        } else {
            this.isShowSettingsBar = true
        }
    }

    @action.bound
    public changeProfilePhoto(target: HTMLInputElement) {
        const data = new FormData()
        const file = target.files ? target.files[0] : undefined
        if (file) {
            data.append('file', file)
            addProfilePhotoReq(data)
                .then((avatarUrl) => {
                    const _id = this.client.id || ''
                    const data: ISaveProfilePhotoResponse = {
                        avatarUrl,
                        _id,
                    }
                    return saveProfilePhotoReq(data).then(() => {
                        runInAction(() => {
                            this.client.avatarUrl = avatarUrl
                        })
                    })
                })
                .catch((err) => {
                    runInAction(() => {
                        this.mainStore.error = err.error
                    })
                })
        }
    }

    @action.bound
    public saveInputValueNewSettingsForm(prop: keyof IUser, value: string) {
        ;(this.newSettings[prop] as string) = value
    }

    @action.bound
    public submutNewSettings() {
        this.newSettings.id = this.client.id
        updateClientSettings(this.newSettings)
            .then(() => {
                runInAction(() => {
                    this.authClient()
                    this.mainStore.error = ''
                    this.mainStore.success =
                        'Your new settings were successfully updated!'
                })
            })
            .catch((err) => {
                runInAction(() => {
                    this.mainStore.error = err.error
                })
            })
    }

    @action.bound
    public setTheme(theme: string) {
        const data: ISetThemeReq = {
            theme,
            _id: this.client.id || '',
        }

        return setThemeReq(data)
            .then((res) => {
                runInAction(() => {
                    this.client.theme = res.theme
                })
            })
            .catch((err) => {
                runInAction(() => {
                    this.mainStore.error = err.error
                })
            })
    }
}
