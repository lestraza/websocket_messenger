import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { IGetStore } from '../../store/MainStore'
import { action, runInAction } from 'mobx'
import { Link } from 'react-router-dom'
import { RouterProps } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import { IUser } from './store/Auth.interface'
import Error from '../Commons/Error'
import Success from '../Commons/Success'
import { Button } from '../Commons/Button'
import FormInput from '../Commons/FormInput'

export interface IRegisterClientProps extends RouterProps {}

@inject('getStore')
@observer
export default class RegisterClient extends React.Component<
    IRegisterClientProps
> {
    mainStore = this.injected.getStore('mainStore')
    authStore = this.injected.getStore('authStore')

    private get injected() {
        return this.props as IRegisterClientProps & IGetStore
    }

    componentWillUnmount() {
        this.mainStore.error = ''
    }

    @action.bound
    public onChangeSaveValue(e: React.ChangeEvent<HTMLInputElement>) {
        const { saveInputValueRegisterForm } = this.authStore
        const value = e.currentTarget.value
        const prop = e.currentTarget.dataset.id
        if (prop) {
            saveInputValueRegisterForm(prop as keyof IUser, value)
        }
    }

    @action.bound
    onSubmitNewClient(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        const { registerNewClient } = this.authStore
        const {
            name,
            lastname,
            email,
            password,
        } = this.authStore.clientRegisterProps
        if (name && lastname && email && password) {
            registerNewClient()
                .then(() => {
                    this.props.history.push('/signin')
                    this.mainStore.success = 'You have successfully registered!'
                })
                .catch((err) => {
                    runInAction(() => {
                        this.mainStore.error = err.error
                    })
                })
        } else {
            this.mainStore.error = 'Please add all reqiured data!'
        }
    }

    public render() {
        const {
            name,
            lastname,
            email,
            password,
        } = this.authStore.clientRegisterProps
        const { error, success } = this.mainStore
        const { isRegisterLoading } = this.authStore

        return (
            <div className="create-account form">
                <Link
                    to={'/signin'}
                    className="button button--small"
                    style={{ position: 'absolute', left: '40px' }}
                >
                    <FontAwesomeIcon icon={faChevronLeft} color={'#989898'} />
                    &nbsp; Sign In
                </Link>
                <p className="create-account__title">Create your account</p>
                <form
                    className="create-account__form"
                    onSubmit={this.onSubmitNewClient}
                >
                    <div className="form-row">
                        <div className="create-account__subtitle form-label">
                            Name
                        </div>
                        <FormInput
                            type="text"
                            value={name}
                            id="name"
                            onChange={this.onChangeSaveValue}
                            required={true}
                        />
                    </div>
                    <div className="form-row">
                        <div className="create-account__subtitle form-label">
                            Last name
                        </div>
                        <FormInput
                            type="text"
                            id="lastname"
                            value={lastname}
                            onChange={this.onChangeSaveValue}
                            required={true}
                        />
                    </div>
                    <div className="form-row">
                        <div className="create-account__subtitle form-label">
                            Email address
                        </div>
                        <FormInput
                            type="email"
                            id="email"
                            value={email}
                            onChange={this.onChangeSaveValue}
                            required={true}
                        />
                    </div>
                    <div className="form-row">
                        <div className="create-account__subtitle form-label">
                            Password
                        </div>
                        <FormInput 
                            type="password"
                            id="password"
                            value={password}
                            onChange={this.onChangeSaveValue}
                            required={true}
                        />
                    </div>
                    <div className="form-row">
                        <Button
                            text="Register"
                            isPrimary={true}
                            isLoading={isRegisterLoading}
                        />
                    </div>
                </form>

                <Error error={error} />

                <Success success={success} />
                <div className="font-small">
                    By creating an account, you agree to the Terms of Service.
                    For more information about Messenger&apos;s privacy
                    practices, see the Messenger Privacy Statement. We&apos;ll
                    occasionally send you account-related emails.
                </div>
            </div>
        )
    }
}
