import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { IGetStore } from '../../store/MainStore'
import { action, runInAction } from 'mobx'
import { IUser } from './store/Auth.interface'
import Error from '../Commons/Error'
import Success from '../Commons/Success'
import { Button } from '../Commons/Button'
import FormInput from '../Commons/FormInput'

export interface ISignInProps {}
@inject('getStore')
@observer
export default class SignIn extends React.Component<ISignInProps> {
    mainStore = this.injected.getStore('mainStore')
    authStore = this.injected.getStore('authStore')

    private get injected() {
        return this.props as ISignInProps & IGetStore
    }

    componentWillUnmount() {
        this.mainStore.error = ''
    }

    @action.bound
    onChangeSaveValue(e: React.FormEvent<HTMLInputElement>) {
        const { saveInputValueRegisterForm } = this.authStore
        const value = e.currentTarget.value
        const prop = e.currentTarget.dataset.id
        if (prop) {
            saveInputValueRegisterForm(prop as keyof IUser, value)
        }
    }

    @action.bound
    onSubmitLogin(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        this.authStore.isSignLoading = true
        const { clientLogin } = this.authStore
        const { email, password } = this.authStore.clientRegisterProps
        if (email && password) {
            clientLogin().finally(() => {
                runInAction(() => {
                    this.authStore.isSignLoading = false
                })
            })
        } else {
            this.mainStore.error = 'Please add all reqiured data!'
        }
    }
    public render() {
        const { isSignLoading, clientRegisterProps } = this.authStore
        const { email, password } = clientRegisterProps
        const { error, success } = this.mainStore

        return (
            <div className="flex-column">
                <div className="main-back"></div>
                <div className="auth-container form">
                    <p>Welcome to Messenger</p>
                    <form className="auth-form" onSubmit={this.onSubmitLogin}>
                        <div className="form-row">
                            <div className="form-label">Email address</div>
                            <FormInput
                                type="email"
                                id="email"
                                value={email}
                                onChange={this.onChangeSaveValue}
                                required={true}
                            />
                        </div>
                        <div className="form-row">
                            <div className="form-label">Password</div>
                            <FormInput
                                type="password"
                                id="password"
                                value={password}
                                onChange={this.onChangeSaveValue}
                                required={true}
                            />
                        </div>
                        <div className="form-row">
                            <Button
                                text={'Sign in'}
                                isPrimary={true}
                                isLoading={isSignLoading}
                            />
                        </div>
                    </form>
                    <Error error={error} />
                    <Success success={success} />

                    <div className="create-account">
                        New to messenger?&nbsp;
                        <Link to={'/register'}>Create an account</Link>
                    </div>
                </div>
            </div>
        )
    }
}
